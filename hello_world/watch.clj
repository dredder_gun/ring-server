(require 'cljs.build.api)

(cljs.build.api/build "src"
  { :main 'hello-world.core
    :asset-path "js"
    :output-to "../resources/public/js/main.js"})
