(ns notes.views
  (:require

   ; "Шаблонизатор"
   [selmer.parser :as parser]
   [selmer.filters :as filters]

   ; Время и дата
   [joda-time :as t]

   ; библиотека для отрисовки html вёрстки
   [hiccup.core]
   [hiccup.page]
   [hiccup.form]

   ; Для HTTP заголовков
   [ring.util.response :refer [content-type response]]

   ; Для CSRF защиты
   [ring.util.anti-forgery :refer [anti-forgery-field]]))

   (use 'hiccup.core)
   (use 'hiccup.page)
   (use 'hiccup.form)
   (use 'hiccup.element)

; Подскажем Selmer где искать наши шаблоны
(parser/set-resource-path! (clojure.java.io/resource "templates"))

; Чтобы привести дату в человеко-понятный формат
(defn format-date-and-time
  "Отформатировать дату и время"
  [date]
  (let [formatter (t/formatter "yyyy-MM-dd в H:m:s" :date-time)]
    (when date
      (t/print formatter date))))

; Добавим фильтр для использования в шаблоне
(filters/add-filter! :format-datetime
                     (fn [content]
                       [:safe (format-date-and-time content)]))

; Добавим тэг с полем для форм в нем будет находится
; автоматически сгенерированное поле с anti-forgery ключом
(parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))

(defn render [template & [params]]
  "Эта функция будет отображать наши html шаблоны
  и передавать в них данные"
  (-> template
      (parser/render-file

        ; Добавим к получаемым данным постоянные
        ; значения которые хотели бы получать
        ; на любой странице
        (assoc params
          :title "Менеджер заметок"
          :page (str template)))

      ; Из всего этого сделаем HTTP ответ
      response
      (content-type "text/html; charset=utf-8")))

(defn note
  "Страница просмотра заметки"
  [note]

  (html
    [:div.container
      [:ul [:li [:a {:href "http://localhost:3000/"} "На главную"]]]
      [:h1#main-task-header (:name note)]
      [:div.container-codesample
        [:pre
          [:code.clojure (:code note)]]
        [:div.devider]
        [:div.demonstration (:output note)]]]))

(defn edit
  "Страница редактирования заметки"
  [note]
  (render "edit.html"

          ; Передаем данные в шаблон
          {:note note}))

(defn create
  "Страница создания заметки"
  []
  ; (render "create.html")

  (html
    [:h1 "Создать заметку"]
    [:form {:novalidate "" :role "form" :method "POST" :action "/create"}
    (anti-forgery-field)
    [:p
     (label "header" "Заголовок")
     (text-field {:placeholder "Заголовок"} "header")]
    [:p
     (label "letter" "Заметка")
     (text-area {:placeholder "Текст заметки"} "letter")]
    [:div [:input {:type "submit" :value "Создать"}]]]))

(defn pagination
  []
  [:nav#pagination-wrapper {:aria-label "Page navigation"}
    [:ul.pagination
      [:li
        [:a {:href "#" :aria-label "Previous"} [:span {:aria-hidden true} "&laquo;"]]]
      [:li [:a {:href "#"} "1"]]
      [:li [:a {:href "#"} "2"]]
      [:li
        [:a {:href "#" :aria-label "Next"} [:span {:aria-hidden true} "&raquo;"]]]]])

(defn search-form
  []
  [:div.row
    [:div.col-lg-6
      [:div.input-group
        [:input.form-control {:type "text" :placeholder "Поиск функции в задачах"}]
        [:span.input-group-btn [:button.btn.btn-default {:type "button"} "Искать!"]]]]])

(defn main
  [notes]
    (html
      [:div#container-main.container
        [:h1#main-header "Читшит по ЯП Clojure"]
        [:ul (map #(do [:li [:a {:href (str "/note/" (:_id %))}(:name %)]]) notes)]
    (pagination)
    (search-form)]))

; переделал функцию include-css-custom,
; т.к. в ней ссылки на стили вставлялись от текушего url

(defn include-css-custom
  "Include a list of external stylesheet files."
  [& styles]
  (for [style styles]
[:link {:type "text/css", :href (str "http://localhost:3000/" style), :rel "stylesheet"}]))

(defn index
  "Главная страница приложения. layout"
  [title variable-body]
    (html5
      {:lang "en"}
      [:head
        (include-css-custom "css/bootstrap.min.css" "css/screen.css" "css/problem-page.css")
        (include-js "js/main.js")
        [:title title]]
      [:body variable-body]))

; как работает деструктуризация
(defn smthg
  [x]
  [:ok (* 2 x) (pos? x)])

(let [[one two three] (smthg 9)]
  (println one)
  (println two)
  (println three))

(def m {:name "Vanya" :age 17})
(let [{n :name, a :age} m]
  [n a])

(defn fun2 [[x y z]]
  (println x)
  (println y)
  (println z))

(fun2 [:a 2 :b 3 :c 4])
