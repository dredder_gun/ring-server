(ns stylesheet.css
  (:require [garden.def :refer [defstylesheet defstyles]]
            [garden.units :refer [px]]))

;; Change defstylesheet to defstyles.
(defstyles screen
  [:.header {
    :color "red"
  }]

  [:body
   {:font-family "sans-serif"
    :font-size (px 16)
    :line-height 1.5}])

(defstyles problem-page
  [:#main-task-header {:text-align "center"}]
  [:.devider {:background-color "#ccc" :width "1px" :min-height (px 200) :height "100%"}]
  [:.container-codesample {:padding-top (px 50) :display "flex" :justify-content "space-around"}]
  [:#main-header {:font-size (px 30) :font-family "sans-serif"}])
