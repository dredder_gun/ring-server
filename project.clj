(defproject cash-money "1.1.1"
  :dependencies [[org.clojure/clojure "1.6.0"]
                  [compojure "1.3.1"]
                  [ring/ring-defaults "0.1.5"]
                  [selmer "0.8.2"]
                  [com.novemberain/monger "2.0.1"]
                  [clojure.joda-time "0.6.0"]]
  :plugins [[lein-ring "0.8.13"]
            [lein-garden "0.3.0"]]
  :ring {:handler notes.handler/app}
  :profiles {:dev
             {:dependencies
              [[javax.servlet/servlet-api "2.5"]
              [ring-mock "0.1.5"]]}}
  :garden {:builds [{;; Optional name of the build:
                     :id "screen"
                     ;; Source paths where the stylesheet source code is
                     :source-paths ["src"]
                     ;; The var containing your stylesheet:
                     :stylesheet stylesheet.css/screen
                     ;; Compiler flags passed to `garden.core/css`:
                     :compiler {;; Where to save the file:
                                :output-to "resources/public/css/screen.css"
                                ;; Compress the output?
                                :pretty-print? false}}
                     {
                     :id "problem-page"
                     :source-paths ["src"]
                     :stylesheet stylesheet.css/problem-page
                     :compiler {
                                :output-to "resources/public/css/problem-page.css"
                                :pretty-print? false}}]})
  ;:garden {:builds []})
